# Kubernetes-Docker
REPO for configuration of cBioPortal on k8s cluster

CI -> manual


## Build
    - Job image (using in API)
    - Cbio db image (using in API)

## Deploy
    - Deploy all necessary configuration to k8s for cbio-on-demand
    - ! Run deploy in "secure-routing" and "api" repositories !
